﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace TICProject
{
	class InfoAmountLogic
	{
		private Dictionary<char, int> _alphabet;
		public ObservableCollection<ParamValuePair<double, char>> Frequency { get; set; }

		public InfoAmountLogic(Languages selectedLang)
		{
			_alphabet = new Dictionary<char, int>();
			Frequency = new ObservableCollection<ParamValuePair<double, char>>();
			InitializeAlphabet(selectedLang);
		}

		public void Update(Languages selectedLang)
		{
			_alphabet.Clear();
			Frequency.Clear();
			InitializeAlphabet(selectedLang);
		}

		public double AnalyzeData(string data)
		{
			CountFrequency(data);
			return InformationAmount(data);
		}

		private double InformationAmount(string data)
		{
			return -data.Length * Frequency.Where(t => t.Param != 0).Sum(t => t.Param*Math.Log(t.Param, 2));
		}

		private void CountFrequency(string text)
		{
			text = text.ToLower();
			foreach (char ch in text)
			{
				if (_alphabet.ContainsKey(ch))
				{
					_alphabet[ch]++;
				}
			}
			foreach (var tmp in _alphabet) Frequency.Add(new ParamValuePair<double, char>
			{
				Param = (tmp.Value * 1.0 / text.Length), Value = tmp.Key
			});
			Frequency = new ObservableCollection<ParamValuePair<double, char>>(Frequency.OrderByDescending(y => y.Param));
		}

		private void InitializeAlphabet(Languages selectedLang)
		{
			switch (selectedLang)
			{
				case Languages.English: English(); break;
				case Languages.Russian: Russian(); break;
				case Languages.Ukrainian: Ukrainian(); break;
			}
		}

		private void English()
		{
			for (int i = 0; i < 26; i++)
			{
				_alphabet.Add((char)(i + 'a'), 0);
			}
		}

		private void Russian()
		{
			for (int i = 0; i < 32; i++)
			{
				_alphabet.Add((char)(i + 'а'), 0);
			}
			_alphabet.Add('ё', 0);
		}

		private void Ukrainian()
		{
			Russian();
			_alphabet.Remove('ё');
			_alphabet.Remove('ъ');
			_alphabet.Remove('ы');
			_alphabet.Remove('э');
			_alphabet.Add('ї', 0);
			_alphabet.Add('є', 0);
			_alphabet.Add('і', 0);
			_alphabet.Add('ґ', 0);
		}
	}
}
