﻿
namespace TICProject
{
	class ParamValuePair<T1, T2>
	{
		public T1 Param { get; set; }
		public T2 Value { get; set; }
	}
}
