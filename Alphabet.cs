﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TICProject
{
    class Alphabet
    {
        private Dictionary<char,double> probability = new Dictionary<char,double>();
        public Alphabet()
        {

            probability['а'] = 0.075;//a
            probability['б'] = 0.017; //б
            probability['в'] = 0.046; //в
            probability['г'] = 0.016; //г
            probability['д'] = 0.03;//д
            probability['е'] = 0.087; //е
            probability['ё'] = 0.087;//ё
            probability['ж'] = 0.009;//ж
            probability['з'] = 0.018;//з
            probability['и'] = 0.075;//и
            probability['й'] = 0.012;//й
            probability['к'] = 0.034;//к
            probability['л'] = 0.042;//л
            probability['м'] = 0.031;//м
            probability['н'] = 0.065;//н
            probability['о'] = 0.11;//о
            probability['п'] = 0.028;//п
            probability['р'] = 0.048;//р
            probability['с'] = 0.055;//с
            probability['т'] = 0.065;//т
            probability['у'] = 0.025;//у
            probability['ф'] = 0.002;//ф
            probability['х'] = 0.011;//х
            probability['ц'] = 0.005;//ц
            probability['ч'] = 0.015;//ч
            probability['ш'] = 0.007;//ш
            probability['щ'] = 0.004;//щ
            probability['ь'] = 0.017;//ь
            probability['ъ'] = 0.017;//ъ
            probability['ы'] = 0.019;//ы
            probability['э'] = 0.003;//э
            probability['ю'] = 0.022;//ю
            probability['я'] = 0.022;//я
            probability[' '] = 0;
        }

       
             
        public double getProbability(char i){
            return probability[i];
    }

        public int getIntProbability(char i)
        {
            return (int)(probability[i] * 3200);
        }



    }
}
