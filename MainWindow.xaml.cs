﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace TICProject
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<double> cum_freq;
        double range, high, low ;


        private void submit_click(object sender, RoutedEventArgs e)
        {
            high = 1;
            low = 0;
            cum_freq = new List<double>();
            var alphabet = new TICProject.Alphabet();
            String input_text = text_area.Text.ToLower();
            cum_freq.Add(1.0);
            for(int i=0;i<input_text.Length;i++){
                cum_freq.Add(alphabet.getProbability(input_text[i]));
                encode_symbol(i+1);
            }
            
            text_code_float.Content = "[" + low.ToString() + " : " + high.ToString() + ")";
            text_code_fractional.Content = "[" + ((long)(low * 3200000000)).ToString() + "/3200000000 : " + ((long)(high * 3200000000)).ToString() + "/3200000000 )";
        }

        private void encode_symbol(int i){
     range = high - low;
     high  = low + range*cum_freq[i-1];
     low   = low + range*cum_freq[i];
        }
    }
}
